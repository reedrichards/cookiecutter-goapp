#  {{ cookiecutter.project_name }}

- https://{{ cookiecutter.gitlab_domain }}/{{ cookiecutter.gitlab_account }}/{{ cookiecutter.project_name }}

## Quickstart

if you have [just](https://github.com/casey/just) and [docker](https://docs.docker.com/get-docker/) installed, you can
start the project with `just run`. Otherwise run  

```shell
$ docker run -p 8000:8000 {{ cookiecutter.project_name }}:latest
```

## Setup and Configuration

create an iam user with the following permissions for terraform:

```json
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Sid":"ListImagesInRepository",
         "Effect":"Allow",
         "Action":[
            "ecr:ListImages"
         ],
         "Resource":"arn:aws:ecs:{{ cookiecutter.aws_region }}:{{ cookiecutter.aws_account_id }}:repository/{{ cookiecutter.project_name }}"
      },
      {
         "Sid":"GetAuthorizationToken",
         "Effect":"Allow",
         "Action":[
            "ecr:GetAuthorizationToken"
         ],
         "Resource":"*"
      },
      {
         "Sid":"ManageRepositoryContents",
         "Effect":"Allow",
         "Action":[
                "ecr:*"
         ],
         "Resource":"arn:aws:ecr:{{ cookiecutter.aws_region }}:{{ cookiecutter.aws_account_id }}:repository/{{ cookiecutter.project_name }}"
      },
     {
       "Sid": "VisualEditor0",
       "Effect": "Allow",
       "Action": [
         "apprunner:ListConnections",
         "apprunner:ListAutoScalingConfigurations",
         "apprunner:ListServices",
         "iam:*"

       ],
       "Resource": "*"
     },
     {
       "Sid": "VisualEditor1",
       "Effect": "Allow",
       "Action": "apprunner:*",
       "Resource": [
         "arn:aws:apprunner:{{ cookiecutter.aws_region }}:{{ cookiecutter.aws_account_id }}:connection/*/*",
         "arn:aws:apprunner:{{ cookiecutter.aws_region }}:{{ cookiecutter.aws_account_id }}:autoscalingconfiguration/*/*/*",
         "arn:aws:apprunner:{{ cookiecutter.aws_region }}:{{ cookiecutter.aws_account_id }}:service/*/*"
       ]
     }
   ]
}
```

example terraform 

```hcl
resource "aws_iam_user" "{{ cookiecutter.project_name }}" {
  name = "{{ cookiecutter.project_name }}"

  tags = {
    Project  = "{{ cookiecutter.project_name }}"
    Type  = "terraform"
  }
}

data "template_file" "{{ cookiecutter.project_name }}" {
  template = file("./policies/{{ cookiecutter.project_name }}.json")
} 
resource "aws_iam_user_policy" "{{ cookiecutter.project_name }}" {
  name = "{{ cookiecutter.project_name }}"
  user = aws_iam_user.{{ cookiecutter.project_name }}.name

  policy = data.template_file.{{ cookiecutter.project_name }}.rendered
}

```

configure {{ cookiecutter.gitlab_url }}/{{ cookiecutter.gitlab_account }}/{{ cookiecutter.project_name }}/-/settings/ci_cd
for with access key terraform user 

create access key https://console.aws.amazon.com/iam/home#/users/{{ cookiecutter.project_name }}?section=security_credentials

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

push this repo to the new project on gitlab 

```shell
git init --initial-branch=main
git remote add origin git@{{ cookiecutter.gitlab_domain }}:{{ cookiecutter.gitlab_account}}/{{ cookiecutter.project_name}}.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

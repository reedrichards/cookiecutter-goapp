terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.20.0"
    }
  }

  required_version = "~> 1.0"
  backend "http" {

  }
}

variable "aws_region" {
  description = "region to create aws app runner service & ecr repo"
  default = "{{ cookiecutter.aws_region }}"
}
variable "image_tag"  {
  description = "image tag to be deployed to aws app runner"
  default = "latest"
}

provider "aws" {
  region = var.aws_region
}


resource "aws_ecr_repository" "{{ cookiecutter.project_name }}" {
  name                 = "{{ cookiecutter.project_name }}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_apprunner_service" "{{ cookiecutter.project_name }}" {
  service_name = "{{ cookiecutter.project_name }}"

  source_configuration {
    authentication_configuration {
	  access_role_arn = "arn:aws:iam::{{ cookiecutter.aws_account_id }}:role/service-role/AppRunnerECRAccessRole"
	}
    image_repository {
      image_configuration {
        port = "8000"
      }
	  image_identifier      = "${aws_ecr_repository.{{ cookiecutter.project_name }}.repository_url}:${var.image_tag}"
      image_repository_type = "ECR"
    }
  }
}
